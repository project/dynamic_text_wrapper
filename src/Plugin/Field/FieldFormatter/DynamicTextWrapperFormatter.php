<?php

namespace Drupal\dynamic_text_wrapper\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'Random_default' formatter.
 *
 * @FieldFormatter(
 *   id = "dynamic_text_wrapper_formatter",
 *   label = @Translation("Dynamic Text Wrapper Formatter"),
 *   field_types = {
 *     "text",
 *     "text_long",
 *     "text_with_summary"
 *   }
 * )
 */
class DynamicTextWrapperFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = $this->t('Dynamic formatter');
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];
    $config = $this->getThirdPartySettings('dynamic_text_wrapper');
    if (!$config) {
      return $items;
    }
    $textWrapperConfig = $config['wrapper'];
    $textWrapperPluginManager = \Drupal::service('plugin.manager.textwrapper');
    $textWrapperDefinition = $textWrapperPluginManager->getDefinition($textWrapperConfig);
    $textWrapperClass = $textWrapperPluginManager->createInstance($textWrapperConfig);
    foreach ($items as $delta => $item) {
      $format = $item->getValue()['format'];
      // Render each element as markup.
      $element[$delta] = [
        '#type' => 'processed_text',
        '#text' => $textWrapperClass->wrapMarkup($item->value),
        '#format' => $format,
      ];
    }
    return $element;
  }

}
