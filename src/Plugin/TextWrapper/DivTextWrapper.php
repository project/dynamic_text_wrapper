<?php

namespace Drupal\dynamic_text_wrapper\Plugin\TextWrapper;

use Drupal\Component\Utility\Html;
use Drupal\dynamic_text_wrapper\TextWrapperInterface;
use Drupal\dynamic_text_wrapper\TextWrapperBase;
use Wa72\HtmlPageDom\HtmlPageCrawler;
/**
 * @TextWrapper(
 *   id = "div_wrapper",
 *   label = "Div wrapper",
 *   description = @Translation("Wraps markup in a div."),
 * )
 */

class DivTextWrapper extends TextWrapperBase implements TextWrapperInterface {

  public function wrapMarkup($markup) {
    $domHTML = Html::load($markup);
    $newStructure = new \DOMDocument();
    $body = $domHTML->getElementsByTagName('body');
    $nodeList = $body->item(0);
    $currentlyWrapping = false;
    $currentDiv = false;
    $count = 1;
    foreach ($nodeList->childNodes as $node) {
      if ($node->nodeName == 'h2') {
        $currentlyWrapping = true;
        $node->setAttribute('id', 'heading-' . $count);
        $currentDiv = $newStructure->createElement('div');
        $currentDiv->setAttribute('class', "section section-$count");
        $newStructure->appendChild($currentDiv);
        $newH2 = $newStructure->importNode($node, true);
        $currentDiv->appendChild($newH2);
        $count++;
      }
      else if (!$currentlyWrapping) {
        $newNode = $newStructure->importNode($node, true);
        $newStructure->appendChild($newNode);
      }
      else {
        $newNode = $newStructure->importNode($node, true);
        $currentDiv->appendChild($newNode);
      }
    }
    return $newStructure->saveHTML();
  }


}
