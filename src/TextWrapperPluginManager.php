<?php

namespace Drupal\dynamic_text_wrapper;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\dynamic_text_wrapper\Annotation\TextWrapper;
use Drupal\dynamic_text_wrapper\TextWrapperInterface;

/**
 * A plugin manager for TextWrapper plugins.
 */
class TextWrapperPluginManager extends DefaultPluginManager {

  /**
   * Creates the discovery object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    // Plugin dir.
    $subdir = 'Plugin/TextWrapper';
    // Plugin interface.
    $plugin_interface = TextWrapperInterface::class;
    // The name of the annotation class that contains the plugin definition.
    $plugin_definition_annotation_name = TextWrapper::class;
    parent::__construct($subdir, $namespaces, $module_handler, $plugin_interface, $plugin_definition_annotation_name);
    // Alter hook.
    $this->alterInfo('text_wrapper_info');
    // Cache backend for the plugin.
    $this->setCacheBackend($cache_backend, 'text_wrapper_info', ['text_wrapper_info']);
  }

}
