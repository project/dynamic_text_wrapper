<?php

namespace Drupal\dynamic_text_wrapper;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Executable\ExecutableInterface;

/**
 * Provides an interface for a TextWrapper plugin.
 *
 * @see plugin_api
 */
interface TextWrapperInterface extends PluginInspectionInterface {

}
