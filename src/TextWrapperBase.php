<?php

namespace Drupal\dynamic_text_wrapper;

use Drupal\Core\Plugin\PluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a base implementation for a TextWrapper plugin.
 *
 * @see plugin_api
 */
abstract class TextWrapperBase extends PluginBase implements TextWrapperInterface {

  /**
   * Creates a TextWrapper instance.
   *
   * @inheritdoc
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->setConfiguration($configuration);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * Options used by the chunker methods.
   *
   * @return array
   *   The default settings used by 'custom' method.
   */
  public function defaultConfiguration() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $configuration += $this->defaultConfiguration();
    $this->configuration = $configuration;
    return $this;
  }

  public function wrapMarkup($markup) {
    $markup = '<div class="dynamic wrapper">' . $markup . '</div>';
    return $markup;
  }
}
