<?php

namespace Drupal\dynamic_text_wrapper\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a TextWrapper annotation object.
 *
 * Plugin Namespace: Plugin\ChunkerMethod
 *
 * @see \Drupal\dynamic_text_wrapper\TextWrapperInterface
 * @see \Drupal\dynamic_text_wrapper\TextWrapperBase
 * @see plugin_api
 *
 * @Annotation
 */
class TextWrapper extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the plugin.
   *
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $label;

  /**
   * The human-readable description of the plugin.
   *
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $description;
}
